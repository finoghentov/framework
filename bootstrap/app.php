<?php

use Finoghentov\Container\Container;
use Finoghentov\Foundation\Application;
use Finoghentov\Foundation\Contracts\Http\KernelContract;
use Finoghentov\Foundation\Http\Kernel;
use Finoghentov\Routing\Contracts\RouterContract;
use Finoghentov\Routing\Routers\SymfonyRouter;

$app = new Application(Container::init(), dirname(__DIR__));

$app->container->bind(KernelContract::class, Kernel::class);

$app->container->bind(RouterContract::class, SymfonyRouter::class, true);

return $app;
