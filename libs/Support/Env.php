<?php

namespace Finoghentov\Support;

class Env
{
    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public static function get(string $key, $default = null)
    {
        $value = getenv($key);

        if (!$value) {
            $value = $default;
        }

        switch ($value) {
            case 'true':
                return true;
            case 'empty':
                return '';
            case 'null':
                return null;
            default:
                return $value;
        }
    }
}
