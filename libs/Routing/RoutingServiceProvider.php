<?php

namespace Finoghentov\Routing;

use Finoghentov\Foundation\ServiceProvider;

class RoutingServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom($this->app->getRoutesPath());
    }
}
