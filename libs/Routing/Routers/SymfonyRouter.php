<?php

namespace Finoghentov\Routing\Routers;

use Finoghentov\Foundation\Application;
use Finoghentov\Routing\Contracts\RouterContract;
use Finoghentov\Routing\Route;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

class SymfonyRouter implements RouterContract
{
    /**
     * @var Application
     */
    protected Application $app;

    /**
     * @var RouteCollection
     */
    public RouteCollection $routeCollection;

    /**
     * SymfonyRouter constructor.
     * @param Application $app
     * @param RouteCollection $routeCollection
     */
    public function __construct(
        Application $app,
        RouteCollection $routeCollection
    ) {
        $this->app = $app;
        $this->routeCollection = $routeCollection;
        $this->app->container->instance(RouterContract::class, $this);
        $this->app->container->alias('router', RouterContract::class);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function dispatch(Request $request): Response
    {
        $route = $this->getRouteFromRequest($request);

        return $this->prepareResponse($request, $this->runRoute($route));
    }

    /**
     * @param Route $route
     * @return mixed
     */
    protected function runRoute(Route $route)
    {
        return $route->run();
    }

    /**
     * @param Request $request
     * @param $data
     * @return Response
     */
    protected function prepareResponse(Request $request, $data): Response
    {
        return new Response($data, 200);
    }

    /**
     * @param Request $request
     * @return Route
     */
    protected function getRouteFromRequest(Request $request): Route
    {
        $matches = $this->buildUrlMatcher($request)->match($request->getPathInfo());

        /**
         * @var Route $route
         */
        $route = $this->routeCollection->get($matches['_route']);
        $route->setMatches($matches);

        return $route;
    }

    /**
     * @param Request $request
     * @return UrlMatcher
     */
    protected function buildUrlMatcher(Request $request): UrlMatcher
    {
        $context = new RequestContext();
        $context->fromRequest($request);

        return new UrlMatcher($this->routeCollection, $context);
    }

    /**
     * @param string|string[] $path
     */
    public function loadRoutesFrom($path): void
    {
        $path = is_array($path) ? $path : [$path];

        foreach ($path as $routePath) {
            foreach (Finder::create()->files()->name('*.php')->in($routePath) as $routeFile) {
                require $routeFile->getPathname();
            }
        }
    }
}
