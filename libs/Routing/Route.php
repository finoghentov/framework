<?php

namespace Finoghentov\Routing;

use Closure;
use Finoghentov\Container\Container;
use Finoghentov\Foundation\Application;
use Finoghentov\Routing\Routers\SymfonyRouter;
use Symfony\Component\Routing\Route as BaseRoute;
use Symfony\Component\Routing\RouteCollection;

/**
 * @method static static get(string $path, $uses, array $defaults = [], array $requirements = [], array $options = [], $host = '', $schemes = [], $methods = [], $condition = '');
 * @method static static post(string $path, $uses, array $defaults = [], array $requirements = [], array $options = [], $host = '', $schemes = [], $methods = [], $condition = '');
 * @method static static put(string $path, $uses, array $defaults = [], array $requirements = [], array $options = [], $host = '', $schemes = [], $methods = [], $condition = '');
 * @method static static patch(string $path, $uses, array $defaults = [], array $requirements = [], array $options = [], $host = '', $schemes = [], $methods = [], $condition = '');
 * @method static static delete(string $path, $uses, array $defaults = [], array $requirements = [], array $options = [], $host = '', $schemes = [], $methods = [], $condition = '');
 */
class Route extends BaseRoute
{
    /**
     * @var array
     */
    private array $middlewares = [];

    /**
     * @var string|array|Closure
     */
    private Closure|string|array $uses;

    /**
     * Controller that should execute action
     *
     * @var object
     */
    protected object $controller;

    /**
     * Action that should be executed
     *
     * @var string|null
     */
    protected null|string $action;

    /**
     * Route Matching params
     *
     * @var array
     */
    protected array $matches;

    /**
     * Name that represents id in Route Collection
     *
     * @var string
     */
    protected string $name;

    /**
     * Route constructor.
     * @param string $path
     * @param string|array|Closure $uses
     * @param array $defaults
     * @param array $requirements
     * @param array $options
     * @param string $host
     * @param array $schemes
     * @param array $methods
     * @param $condition
     */
    public function __construct(
        string $path,
        $uses,
        array $defaults = [],
        array $requirements = [],
        array $options = [],
        $host = '',
        $schemes = [],
        $methods = [],
        $condition = ''
    ) {
        $this->uses = $uses;
        parent::__construct($path, $defaults, $requirements, $options, $host, $schemes, $methods, $condition);
    }

    /**
     * @return mixed
     */
    public function run()
    {
        if ($this->usesController()) {
            return $this->runController();
        }

        return $this->runClosure();
    }

    /**
     * Run route closure action
     *
     * @return mixed
     */
    protected function runClosure()
    {
        $closure = $this->uses;

        return Application::getInstance()->container->call($closure);
    }

    /**
     * Check if route using controller
     *
     * @return bool
     */
    protected function usesController(): bool
    {
        if (is_callable($this->getUses())) {
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function runController(): mixed
    {
        [$controller, $action] = $this->getControllerAction();

        if (!$action) {
            return Application::getInstance()->container->call([$controller], $this->getVariables());
        }

        return Application::getInstance()->container->call([$controller, $action], $this->getVariables());
    }

    /**
     * @param array $matches
     */
    public function setMatches(array $matches)
    {
        $this->matches = $matches;
    }

    /**
     * @return array
     */
    protected function getControllerAction(): array
    {
        if (isset($this->controller) && isset($this->action)) {
            return [$this->controller, $this->action];
        }

        $uses = $this->getUses();

        if (is_array($uses)) {
            $className = $uses[0];
            $action = $uses[1] ?? null;
        } elseif (is_string($uses)) {
            $segments = explode('@', $uses);
            $className = $segments[0];
            $action = $segments[1] ?? null;
        } else {
            throw new \InvalidArgumentException('$uses should be string|array|closure');
        }

        $this->controller = Application::getInstance()->container->make($className);
        $this->action = $action;

        return [$this->controller, $this->action];
    }

    /**
     * @return callable|array|Closure|string
     */
    public function getUses(): callable|array|Closure|string
    {
        return $this->uses;
    }

    /**
     * @param array|string $middlewares
     * @return Route
     */
    public function withMiddleware(array|string $middlewares): Route
    {
        $middlewares = is_array($middlewares) ? $middlewares : [$middlewares];

        $this->middlewares = array_diff($middlewares, $this->middlewares);

        return $this;
    }

    /**
     * @param RouteCollection $collection
     * @param string|null $name
     */
    public function toCollection(RouteCollection $collection, string $name = null)
    {
        if (!$name) {
            $name = $this->getPath();
        }

        $collection->add($name, $this);
    }

    /**
     * @return array
     */
    public function getVariables(): array
    {
        $matches = $this->matches;
        $routePathVariables = $this->compile()->getPathVariables();

        $varKeys = array_intersect(array_keys($matches), $routePathVariables);

        return array_filter($matches, function ($key) use ($varKeys) {
            return in_array($key, $varKeys);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @param string $name
     * @return $this
     */
    public function name(string $name): static
    {
        /**
         * @var SymfonyRouter $router
         */
        $router = Container::getInstance()->get('router');

        $router->routeCollection->remove($this->name);
        $router->routeCollection->add($name, $this);
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return Route
     */
    public static function __callStatic(string $name, array $arguments)
    {
        /**
         * @var SymfonyRouter $router
         */
        $router = Container::getInstance()->get('router');

        $methods = ['get', 'post', 'put', 'patch', 'delete'];

        if (!in_array($name, $methods)) {
            throw new \BadMethodCallException('Method [' . self::class . "::$name] not exists.");
        }

        $object = new static(...$arguments);
        $object->setMethods($name);
        $object->name = $object->getPath();
        $router->routeCollection->add($object->getPath(), $object);

        return $object;
    }
}
