<?php

namespace Finoghentov\Routing\Contracts;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface RouterContract
{
    /**
     * @param Request $request
     * @return Response
     */
    public function dispatch(Request $request): Response;

    /**
     * @param string|string[] $path
     */
    public function loadRoutesFrom($path): void;
}
