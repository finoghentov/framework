<?php

namespace Finoghentov\View;

use Finoghentov\View\Contracts\EngineContract;

class Renderer
{
    /**
     * @var EngineContract
     */
    protected EngineContract $engine;

    /**
     * @return mixed
     */
    public function render()
    {
        return $this->engine->render();
    }
}
