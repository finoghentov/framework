<?php

namespace Finoghentov\View\Contracts;

interface FactoryContract
{
    /**
     * @return EngineContract
     */
    public function create(): EngineContract;
}
