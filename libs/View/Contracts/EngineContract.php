<?php

namespace Finoghentov\View\Contracts;

interface EngineContract
{
    /**
     * @return mixed
     */
    public function render();
}
