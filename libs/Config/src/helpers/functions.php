<?php

if (!function_exists('config')) {
    /**
     * @param string|null $path
     * @return mixed
     */
    function config(string $path = null): mixed
    {
        if (!$path) {
            return app('config');
        }

        return app('config')?->get($path);
    }
}
