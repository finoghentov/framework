<?php

namespace Finoghentov\Config;

use Finoghentov\Config\Contracts\RepositoryContract;

class Repository implements RepositoryContract
{
    /**
     * @var array
     */
    protected array $items;

    /**
     * Repository constructor.
     *
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed
     */
    public function get(string $key, mixed $default = null): mixed
    {
        if (!strstr($key, '.')) {
            return $this->items[$key] ?? $default;
        } else {
            $value = $this->items;

            foreach (explode('.', $key) as $segment) {
                if (!isset($value[$segment])) {
                    return $default;
                }

                $value = $value[$segment];
            }
        }

        return $value;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, mixed $value)
    {
        $this->items[$key] = $value;
    }
}
