<?php

namespace Finoghentov\Config\Contracts;

interface RepositoryContract
{
    /**
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function get(string $key, mixed $default): mixed;

    /**
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, mixed $value);
}
