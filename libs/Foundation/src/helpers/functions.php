<?php

use Finoghentov\Foundation\Application;

if (!function_exists('app')) {
    /**
     * @param null $abstract
     * @param array $params
     * @return mixed
     */
    function app($abstract = null, array $params = []): mixed
    {
        if (!$abstract) {
            return Application::getInstance();
        }

        return Application::getInstance()->container->make($abstract, $params);
    }
}
