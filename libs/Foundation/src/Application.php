<?php

namespace Finoghentov\Foundation;

use Finoghentov\Container\Container;
use Finoghentov\Container\Contracts\ContainerInterface;

class Application
{
    /**
     * @var ContainerInterface
     */
    public ContainerInterface $container;

    /**
     * @var Application
     */
    protected static Application $instance;

    /**
     * @var array
     */
    protected array $registeredProviders = [];

    /**
     * @var string
     */
    protected string $basePath;

    /**
     * @var string[]
     */
    protected array $routesPath;

    /**
     * @var string
     */
    protected string $configPath;

    /**
     * @var bool
     */
    protected bool $hasBeenBootstrapped = false;

    /**
     * Application constructor.
     * @param ContainerInterface $container
     * @param string $basePath
     */
    public function __construct(ContainerInterface $container, string $basePath)
    {
        $this->container = $container;
        $this->setBasePath($basePath);
        $this->registerBaseBindings();
    }

    /**
     * @return string
     */
    public function getBasePath(): string
    {
        return $this->basePath;
    }

    /**
     * @param array $bootstrappers
     */
    public function bootstrapWith(array $bootstrappers): void
    {
        $this->hasBeenBootstrapped = true;
        foreach ($bootstrappers as $bootstrapper) {
            $this->container->make($bootstrapper)->bootstrap($this);
        }
    }

    /**
     * @param string $basePath
     * @return Application
     */
    protected function setBasePath(string $basePath): Application
    {
        $this->basePath = rtrim($basePath, '\/');
        $this->setBaseRoutesPath();
        $this->setConfigPath();

        return $this;
    }

    /**
     * @return Application
     */
    protected function setBaseRoutesPath(): Application
    {
        $this->routesPath = [
            $this->basePath . '/routes/'
        ];

        return $this;
    }

    /**
     * @return $this
     */
    protected function setConfigPath(): Application
    {
        $this->configPath = $this->basePath . '/config/';

        return $this;
    }

    /**
     * @return string[]
     */
    public function getRoutesPath(): array
    {
        return $this->routesPath;
    }

    /**
     * @return string
     */
    public function getConfigPath(): string
    {
        return $this->configPath;
    }

    /**
     * Check if application has already been bootstrapped
     *
     * @return bool
     */
    public function hasBeenBootstrapped(): bool
    {
        return $this->hasBeenBootstrapped;
    }

    /**
     * @param string $basePath
     * @return $this|Application
     */
    public static function getInstance(string $basePath = '')
    {
        if (isset(self::$instance)) {
            return self::$instance;
        }

        return new static(Container::init(), $basePath);
    }

    protected function registerBaseBindings()
    {
        self::setInstance($this);

        $this->container->alias('app', Application::class);
        $this->container->instance(Application::class, $this);
    }

    /**
     * @param Application $application
     * @return Application
     */
    public static function setInstance(Application $application): Application
    {
        return self::$instance = $application;
    }

    /**
     * @param array $providers
     */
    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider) {
            if (!in_array($provider, $this->registeredProviders)) {
                $this->container->make($provider)->register();
                $this->registeredProviders[] = $provider;
            }
        }
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        return $this->container->get($key);
    }
}
