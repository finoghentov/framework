<?php

namespace Finoghentov\Foundation\Contracts\Http;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface KernelContract
{
    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response;
}
