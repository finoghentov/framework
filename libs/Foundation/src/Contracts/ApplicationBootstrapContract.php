<?php

namespace Finoghentov\Foundation\Contracts;

use Finoghentov\Foundation\Application;

interface ApplicationBootstrapContract
{
    public function bootstrap(Application $app);
}
