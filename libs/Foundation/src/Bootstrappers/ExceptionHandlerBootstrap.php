<?php

namespace Finoghentov\Foundation\Bootstrappers;

use Finoghentov\Foundation\Application;
use Finoghentov\Foundation\Contracts\ApplicationBootstrapContract;
use Finoghentov\Support\Env;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class ExceptionHandlerBootstrap implements ApplicationBootstrapContract
{
    public function bootstrap(Application $app)
    {
        if (Env::get('APP_DEBUG')) {
            $whoops = new Run();
            $whoops->pushHandler(new PrettyPageHandler);
            $whoops->register();
        }
    }
}
