<?php

namespace Finoghentov\Foundation\Bootstrappers;

use Doctrine\DBAL\DriverManager;
use Finoghentov\Container\Container;
use Finoghentov\Foundation\Application;
use Finoghentov\Foundation\Contracts\ApplicationBootstrapContract;

class DatabaseBootstrap implements ApplicationBootstrapContract
{
    public function bootstrap(Application $app)
    {
        /**
         * Database
         */
        $connectionParams = [
            'dbname' => 'cscart',
            'user' => 'cscart',
            'password' => 'cscart',
            'host' => '127.0.0.1',
            'port' => '3336',
            'driver' => 'pdo_mysql',
        ];

        Container::getInstance()->bind('db', function () use ($connectionParams) {
            return DriverManager::getConnection($connectionParams);
        });
    }
}
