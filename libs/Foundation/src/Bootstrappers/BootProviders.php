<?php

namespace Finoghentov\Foundation\Bootstrappers;

use Finoghentov\Foundation\Application;
use Finoghentov\Foundation\Contracts\ApplicationBootstrapContract;

class BootProviders implements ApplicationBootstrapContract
{
    /**
     * @param Application $app
     */
    public function bootstrap(Application $app)
    {
        $config = $app->container->get('config');

        $app->registerProviders($config->get('application.providers'));
    }
}
