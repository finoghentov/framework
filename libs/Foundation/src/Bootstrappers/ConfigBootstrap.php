<?php

namespace Finoghentov\Foundation\Bootstrappers;

use Finoghentov\Config\Contracts\RepositoryContract;
use Finoghentov\Config\Repository;
use Finoghentov\Foundation\Application;
use Finoghentov\Foundation\Contracts\ApplicationBootstrapContract;
use Symfony\Component\Finder\Finder;

class ConfigBootstrap implements ApplicationBootstrapContract
{
    public function bootstrap(Application $app)
    {
        /**
         * @var RepositoryContract $repository
         */
        $repository = $app->container->build(Repository::class);

        $app->container->alias('config', RepositoryContract::class);
        $app->container->instance(RepositoryContract::class, $repository);

        $files = $this->getConfigFiles($app);

        foreach ($files as $name => $config) {
            $repository->set($name, require $config);
        }
    }

    /**
     * @param Application $app
     * @return array
     */
    protected function getConfigFiles(Application $app)
    {
        $files = [];

        $configPath = $app->getConfigPath();

        foreach (Finder::create()->files()->name('*.php')->in($configPath) as $file) {
            /**
             * @var \SplFileInfo $file
             */
            $files[$file->getBasename('.php')] = $file->getPathname();
        }

        return $files;
    }
}
