<?php

namespace Finoghentov\Foundation\Bootstrappers;

use Finoghentov\Foundation\Application;
use Finoghentov\Foundation\Contracts\ApplicationBootstrapContract;
use Symfony\Component\Dotenv\Dotenv;

class EnvBootstrap implements ApplicationBootstrapContract
{
    public function bootstrap(Application $app)
    {
        $env = new Dotenv();
        $env->usePutenv();
        $env->load($app->getBasePath() . '/.env');
    }
}
