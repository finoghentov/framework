<?php

namespace Finoghentov\Foundation;

use Finoghentov\Routing\Contracts\RouterContract;

abstract class ServiceProvider
{
    /**
     * @var Application
     */
    protected Application $app;

    /**
     * ServiceProvider constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Registering providers
     * Fired before sending request through router
     */
    public function register(): void
    {

    }

    /**
     * @param string|string[] $path
     */
    protected function loadRoutesFrom($path): void
    {
        /**
         * @var RouterContract $router
         */
        $router = $this->app->container->make(RouterContract::class);

        $router->loadRoutesFrom($path);
    }
}
