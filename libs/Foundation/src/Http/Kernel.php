<?php

namespace Finoghentov\Foundation\Http;

use Finoghentov\Foundation\Application;
use Finoghentov\Foundation\Bootstrappers\BootProviders;
use Finoghentov\Foundation\Bootstrappers\ConfigBootstrap;
use Finoghentov\Foundation\Bootstrappers\EnvBootstrap;
use Finoghentov\Foundation\Bootstrappers\ExceptionHandlerBootstrap;
use Finoghentov\Foundation\Bootstrappers\RegisterProviders;
use Finoghentov\Foundation\Contracts\Http\KernelContract;
use Finoghentov\Routing\Contracts\RouterContract;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Kernel implements KernelContract
{
    /**
     * @var Application $app
     */
    protected Application $app;

    /**
     * @var RouterContract
     */
    protected RouterContract $router;

    /**
     * HttpKernel constructor.
     */
    public function __construct(Application $app, RouterContract $router)
    {
        $this->app = $app;
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $request->enableHttpMethodParameterOverride();

        return $this->sendRequestThroughRouter($request);
    }

    /**
     * @param Request $request
     * @return Response
     */
    protected function sendRequestThroughRouter(Request $request): Response
    {
        $this->app->container->alias('request', Request::class);
        $this->app->container->instance(Request::class, $request);

        $this->bootstrap();

        return $this->router->dispatch($request);
    }

    protected function bootstrap()
    {
        if (!$this->app->hasBeenBootstrapped()) {
            $this->app->bootstrapWith($this->getDefaultBootstrappers());
        }
    }

    /**
     * @return string[]
     */
    protected function getDefaultBootstrappers(): array
    {
        return [
            EnvBootstrap::class,
            ConfigBootstrap::class,
            ExceptionHandlerBootstrap::class,
            RegisterProviders::class,
            BootProviders::class,
        ];
    }
}
