<?php

use Finoghentov\Support\Env;

return [
    'default' => Env::get('DB_CONNECTION', 'mysql'),

    'connections' => [
        'mysql' => [
            'driver'   => 'mysql',
            'database' => Env::get('DB_NAME', 'database_name'),
            'host'     => Env::get('DB_HOST', 'cscart'),
            'username' => Env::get('DB_USER', 'root'),
            'password' => Env::get('DB_PASSWORD', 'root'),
            'port'     => Env::get('DB_PORT', '3306'),
        ]
    ]
];
