<?php

use App\Controllers\DefaultController;
use Finoghentov\Routing\Route;

Route::get('/', [DefaultController::class, 'index'])->name('index');
Route::get('/category/{slug}', [DefaultController::class])->name('category');

