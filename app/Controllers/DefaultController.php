<?php

namespace App\Controllers;

use Finoghentov\Http\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class DefaultController extends BaseController
{
    public function index()
    {
        $loader = new FilesystemLoader();
        $twig = new Environment();
        dd($twig);
        return 'hello';
    }

    public function __invoke(Request $request, $slug): string
    {
        return $slug;
    }
}
