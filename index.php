<?php

use Finoghentov\Foundation\Application;
use Finoghentov\Foundation\Contracts\Http\KernelContract;
use Symfony\Component\HttpFoundation\Request;

require_once "vendor/autoload.php";

/**
 * @var Application $app
 */
$app = require_once __DIR__.'/bootstrap/app.php';

/**
 * @var KernelContract $kernel
 */
$kernel = $app->container->make(KernelContract::class);

$response = $kernel->handle(
    Request::createFromGlobals()
)->send();
